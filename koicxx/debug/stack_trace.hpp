#ifndef KOICXX_DEBUG_STACK_TRACE_HPP
#define KOICXX_DEBUG_STACK_TRACE_HPP

#include <koicxx/filesystem.hpp>

#include <boost/version.hpp>

#include <boost/lexical_cast.hpp>
# if BOOST_VERSION < 105300
#  include <boost/thread/locks.hpp>
# else // BOOST_VERSION >= 105300
#  include <boost/thread/lock_guard.hpp>
# endif
#include <boost/thread/mutex.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
#  include <Dbghelp.h>
# endif // _MSC_VER

#include <string>
#include <vector>

# ifdef _MSC_VER
#  pragma comment(lib, "Dbghelp.lib")
# endif // _MSC_VER

namespace koicxx {
namespace debug {

#ifdef _MSC_VER

inline
std::vector<std::string> get_call_stack()
{
  // According to MSDN, all DbgHelp functions are single threaded
  // Therefore, calls from more than one thread
  // will likely result in unexpected behavior or memory corruption

  static boost::mutex sync;
  boost::lock_guard<boost::mutex> lock(sync);

  std::vector<std::string> call_stack;

  // According to MSDN, the sum of FramesToSkip and FramesToCapture
  // must be less than 63 in case of Windows Server 2003 and Windows XP
  const ULONG frames_to_skip = 1;
  const ULONG frames_to_capture = 61;
  void* stack[frames_to_capture];

  HANDLE cur_process_handle = GetCurrentProcess();
  if (SymInitialize(cur_process_handle, NULL, TRUE) == FALSE)
  {
    return call_stack;
  }

  USHORT frames = CaptureStackBackTrace(
    frames_to_skip
    , frames_to_capture
    , stack
    , NULL
  );
  for (USHORT i = 0; i < frames; ++i)
  {
    std::string cur_frame_info;

    DWORD_PTR cur_frame = reinterpret_cast<DWORD_PTR>(stack[i]);

    DWORD64 sym_displacement = 0;

    char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(char)];
    PSYMBOL_INFO symbol = reinterpret_cast<PSYMBOL_INFO>(buffer);
    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol->MaxNameLen = MAX_SYM_NAME;

    if (SymFromAddr(
      cur_process_handle
      , cur_frame
      , &sym_displacement
      , symbol
    ) == FALSE)
    {
      cur_frame_info += "[No Symbol]";
    }
    else
    {
      cur_frame_info += symbol->Name;
    }

    DWORD line_displacement = 0;
    IMAGEHLP_LINE64 line = {};
    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    if (SymGetLineFromAddr64(
      cur_process_handle
      , cur_frame
      , &line_displacement
      , &line
    ) == TRUE)
    {
      try
      {
        cur_frame_info += " (" + koicxx::filesystem::get_file_name_without_path(line.FileName)
          + ":" + boost::lexical_cast<std::string>(line.LineNumber) + ")";
      }
      catch (const boost::bad_lexical_cast&) {}
    }

    call_stack.push_back(cur_frame_info);
  }

  // We can't do anything if this function fails,
  // so just ignore the return value
  SymCleanup(cur_process_handle);

  return call_stack;
}

#else // !_MSC_VER

inline
std::vector<std::string> get_call_stack()
{
  return std::vector<std::string>();
}

#endif

} // namespace debug
} // namespace koicxx

#endif // !KOICXX_DEBUG_STACK_TRACE_HPP