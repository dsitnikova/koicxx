#ifndef KOICXX_THREAD_HPP
#define KOICXX_THREAD_HPP

#include <boost/thread.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
# endif // _MSC_VER

#include <ios>
#include <sstream>
#include <string>

namespace koicxx {
namespace thread {

#ifdef _MSC_VER

namespace detail {

// From http://msdn.microsoft.com/en-us/library/xcb2z8hs.aspx
// The only difference is the addition of const-qualifier to the threadName parameter

//
// Usage: SetThreadName (-1, "MainThread");
//
const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
  DWORD dwType; // Must be 0x1000.
  LPCSTR szName; // Pointer to name (in user addr space).
  DWORD dwThreadID; // Thread ID (-1=caller thread).
  DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void SetThreadName(DWORD dwThreadID, const char* threadName)
{
  THREADNAME_INFO info;
  info.dwType = 0x1000;
  info.szName = threadName;
  info.dwThreadID = dwThreadID;
  info.dwFlags = 0;

  __try
  {
    RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
  }
  __except (EXCEPTION_EXECUTE_HANDLER)
  {
  }
}

} // namespace detail

/**
* \brief Sets a thread name
* \note You need to pass a pointer that will be valid for the whole boost::thread object lifetime
*
* \todo Add non-MSVC version
*/
void set_thread_name(const boost::thread& th, const char* name)
{
  DWORD thread_id;
  std::stringstream ss;
  ss << std::hex << th.get_id();
  ss >> thread_id;

  detail::SetThreadName(thread_id, name);
}

#endif // _MSC_VER

} // namespace thread
} // namespace koicxx

#endif // !KOICXX_THREAD_HPP