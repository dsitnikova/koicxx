#ifndef KOICXX_PROPERTY_TREE_HPP
#define KOICXX_PROPERTY_TREE_HPP

#include <koicxx/exception.hpp>

#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <sstream>
#include <string>

namespace koicxx {
namespace property_tree {

class exception : public koicxx::exception {};

enum class format
{
  INI
  , JSON
  , XML
};

/**
 * \brief Creates a boost::property_tree::ptree object from a specified std::string object
 * \throw koicxx::property_tree::exception on failure
 */
inline
boost::property_tree::ptree construct_pt_from_string(const std::string& input, format input_format)
{
  boost::property_tree::ptree pt;
  std::stringstream stream;
  stream << input;

  try
  {
    if (input_format == format::INI)
    {
      boost::property_tree::read_ini(stream, pt);
    }
    else if (input_format == format::JSON)
    {
      boost::property_tree::read_json(stream, pt);
    }
    else if (input_format == format::XML)
    {
      boost::property_tree::read_xml(stream, pt);
    }
  }
  catch (const boost::property_tree::ptree_error& ex)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(make_string() << "An error occurred while parsing input data: " << ex.what());
  }

  return pt;
}

} // namespace property_tree
} // namespace koicxx

#endif // !KOICXX_PROPERTY_TREE_HPP