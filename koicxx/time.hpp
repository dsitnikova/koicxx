#ifndef KOICXX_TIME_HPP
#define KOICXX_TIME_HPP

# ifdef _MSC_VER
#  define _SCL_SECURE_NO_WARNINGS
#  define _CRT_SECURE_NO_WARNINGS
# endif // _MSC_VER

#include <koicxx/make_string.hpp>
#include <koicxx/string.hpp>

#include <boost/date_time/gregorian/greg_date.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>

#include <ctime>
#include <iomanip>
#include <string>
#include <vector>

namespace koicxx {
namespace time {

/**
 * \brief Returns the current time in a std::string object
 */
inline
std::string get_cur_time()
{
  std::time_t t = std::time(0);
  std::string res(std::ctime(&t));
  koicxx::string::remove_trailing_cr_and_lf_characters(res); // We don't need an additional new-line character
  return res;
}

/**
 * \brief Converts the specified tm object to a std::string object according to the format string
 */
inline
std::string convert_tm_to_string(const std::string& fmt, const std::tm& time_info)
{
  std::vector<char> buffer(fmt.size());
  auto len = std::strftime(&buffer[0], buffer.size(), fmt.c_str(), &time_info);
  while (!len)
  {
    buffer.resize(buffer.size() * 2);
    len = std::strftime(&buffer[0], buffer.size(), fmt.c_str(), &time_info);
  }

  std::string res(buffer.begin(), buffer.end());
  koicxx::string::remove_trailing_nulls(res);
  
  return res;
}

/**
 * \brief Returns the current date in a std::string object
 */
inline
std::string get_cur_date(const std::string& fmt)
{
  std::time_t t = std::time(0);
  std::tm* local_time = std::localtime(&t);
  return convert_tm_to_string(fmt, *local_time);
}

inline
std::time_t to_time_t(const boost::posix_time::ptime& t)
{
  boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
  boost::posix_time::time_duration::sec_type x = (t - epoch).total_seconds();
  return std::time_t(x);
}

} // namespace time
} // namespace koicxx

#endif // !KOICXX_TIME_HPP
